let reptile_main = new function () {
  let $this = this;
  let request = require("request");
  let cheerio = require("cheerio");
  let urlMap = {
    "list": "https://www.seasonart.org/reservation/reservation3.php"
  };

  $this.getWeb = function () {
    request({
      url: urlMap.list,
      method: "GET"
    }, function (err, resp, body) {
      if (err || !body) {
        console.log(err);
        return;
      }
      const $ = cheerio.load(body);
      console.log($(".reserv_area1").text().trim())
      let web_str = $(".reserv_area1").text().trim();
      let not_str = "網頁改版中，敬請期待......";
      if (web_str != not_str) {
        telegram_main.setMsg();
      }
    })
  }
  $this.init = function () {
    console.log("Reptile Ready");
  }
  $this.init();
}

let telegram_main = new function () {
  let $this = this;
  let TelegramBotInit = require('node-telegram-bot-api');
  let TelegramToken = '637641354:AAG1VCVqcv6jA_oVBkbBblxLD6dlOvRzOD0';
  let TelegramUserId = "-1001311276594";

  let TelegramBot = new TelegramBotInit(TelegramToken, { polling: true });

  $this.setMsg = function (datas = '') {
    let msg = '';
    if (datas != '') {
      msg = datas;
    } else {
      msg = "四季頁面可以報名了";
    }
    TelegramBot.sendMessage(TelegramUserId, msg);
  }
  $this.init = function () {
    console.log("Telegram Ready");
  }
  $this.init();
}

let schedule_main = new function () {
  let $this = this;
  let schedule = require('node-schedule');
  let rule = new schedule.RecurrenceRule();
  let sec = [0, 10, 20, 30, 40, 50, 60];

  let Run = function () {
    rule.second = sec;
    schedule.scheduleJob(rule, function () {
      reptile_main.getWeb();
      console.log('scheduleCronstyle:' + new Date());
    });
  }

  $this.init = function () {
    console.log("Schedule Ready");
    Run();
  }
}

schedule_main.init();